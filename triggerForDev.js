// triggerForDev.js is only for development in the local PC.
// This is to simulate how AWS Lambda calls node JS program.
// When running this program in the AWS Lambda, this file is not required.
//
// Run "npm install" and then "node triggerForDev.js" in your PC command line to start this program.

const lambdaHandler = require("./index.js");

let event = { action: "stop" };
let context = null;
let callback = (responseError, responseSuccess) => {
  if (responseError) console.log(responseError);
  if (responseSuccess) console.log(responseSuccess);
};

lambdaHandler.handler(event, context, callback);
