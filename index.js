// AWS JavaScript SDK document
// https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/index.html

let AWS = require("aws-sdk");
let nodemailer = require("nodemailer");

// Note1: Credentials and region are required when running program in your local PC.
//        They are not required when running it in the AWS Lambda.
// Note2: Generate AWS access key/secret in the AWS IAM --> Users --> Security Credentials tab
AWS.config.update({
  accessKeyId: "AKIA2L4B3DXSGYGWIQX7",
  secretAccessKey: "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",
  region: "us-east-1"
});

let ec2 = new AWS.EC2();
let rds = new AWS.RDS();
let ses = new AWS.SES();

// Only the EC2 or RDS instance with this tag will be controlled by this program.
// For AWS Aurora DB cluster, this tag should be put on the cluster node rather than on the instance.
const tagStartStop = "Auto-StartStop-Enabled";

exports.handler = async (event, context, callback) => {
  let mailOptions = {
    from: "admin@sgt2.com.br",
    subject: "Erro ao desligar as maquinas de dev e tst",
    html: `<p>As maquinas de dev e tst estão ligadas</p>`,
    to:
      "devalexsandro@gmail.com; rsilveira@4asset.net.br; asantos@4asset.net.br; rklimann@4asset.net.br"
    // bcc: Any BCC address you want here in an array,
  };

  let transporter = nodemailer.createTransport({
    SES: ses
  });

  try {
    let action = event.action.toLowerCase();
    console.log("=== action:", action);

    if (action && ["start", "stop"].indexOf(action) > -1) {
      let resolveOfStartStopEc2Instances = await startStopEc2Instances(action);
      console.log(resolveOfStartStopEc2Instances);

      let resolveOfStartStopRdsDB = await startStopRdsDB(action);
      console.log(resolveOfStartStopRdsDB);
    } else {
      console.log(
        "Action was neither start nor stop. Lambda function aborted."
      );
    }

    console.log("=== Complete");
    return callback(null, "Complete");
  } catch (err) {
    transporter.sendMail(mailOptions, function(err, info) {
      if (err) {
        console.log("Error sending email");
        callback(err);
      } else {
        console.log("Email sent successfully");
        callback();
      }
    });
    return callback(err, null);
  }
};

function startStopEc2Instances(action) {
  return new Promise(async (resolve, reject) => {
    let instanceList = [];

    try {
      console.log("=== Get EC2 instance list.");
      if (action == "start") {
        instanceList = await getEc2InstanceList("stopped");
        console.log(
          "Found " + instanceList.length + " EC2 instances that can be started."
        );
      }
      if (action == "stop") {
        instanceList = await getEc2InstanceList("running");
        console.log(
          "Found " + instanceList.length + " EC2 instances that can be stopped."
        );
      }
    } catch (err) {
      reject(err);
    }

    for (let instance of instanceList) {
      try {
        let instanceNameArray = instance.Tags.filter(t => {
          return t.Key == "Name";
        });
        let instanceName =
          instanceNameArray.length == 0 ? "" : instanceNameArray[0].Value;

        console.log(
          "EC2 instance " +
            instanceName +
            " (" +
            instance.InstanceId +
            ") currently is in " +
            instance.State.Name +
            " status."
        );

        params = {
          InstanceIds: [instance.InstanceId]
          // DryRun: true
        };

        if (action == "stop" && instance.State.Name == "running") {
          await stopEc2Instance(params);
          console.log(
            "Instance " +
              instanceName +
              " (" +
              instance.InstanceId +
              ") is stopped."
          );
        } else if (action == "start" && instance.State.Name == "stopped") {
          await startEc2Instance(params);
          console.log(
            "Instance " +
              instanceName +
              " (" +
              instance.InstanceId +
              ") is started."
          );
        } else {
          console.log(
            "The status of instance " +
              instanceName +
              " (" +
              instance.InstanceId +
              ") is not right for starting/stopping."
          );
        }
      } catch (err) {
        console.log(err);
      }
    }
    resolve("Method startStopEc2Instances() is done.");
  });
}

function getEc2InstanceList(currentInstanceStatus) {
  return new Promise((resolve, reject) => {
    let instanceList = [];
    let params = {
      DryRun: false,
      Filters: [
        {
          Name: "tag-key",
          Values: [tagStartStop]
        },
        {
          Name: "instance-state-name",
          Values: [currentInstanceStatus]
        }
      ]
    };

    ec2.describeInstances(params, function(err, data) {
      if (err) {
        reject(err);
      } else {
        for (let instance of data.Reservations) {
          instanceList.push(instance.Instances[0]);
        }
        resolve(instanceList);
      }
    });
  });
}

function stopEc2Instance(params) {
  return new Promise((resolve, reject) => {
    ec2.stopInstances(params, function(err, data) {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

function startEc2Instance(params) {
  return new Promise((resolve, reject) => {
    ec2.startInstances(params, function(err, data) {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

function startStopRdsDB(action) {
  return new Promise(async (resolve, reject) => {
    let instanceList = [];
    let clusterList = [];
    let startedInstanceCount = (stoppedInstanceCount = startedClusterCount = stoppedClusterCount = 0);

    try {
      console.log("=== Get RDS instance list.");
      instanceList = await getRdsInstanceList();
    } catch (err) {
      reject(err);
    }

    for (let instance of instanceList) {
      try {
        if (
          ["mysql", "mariadb", "postgres", "sqlserver-ex"].indexOf(
            instance.Engine
          ) > -1 ||
          instance.Engine.includes("oracle")
        ) {
          let tags = await getRdsResouceTags(instance.DBInstanceArn);

          for (let tag of tags) {
            if (tag.Key == tagStartStop) {
              console.log(
                "Current status of DB instance " +
                  instance.DBInstanceIdentifier +
                  " is " +
                  instance.DBInstanceStatus
              );

              if (action == "start" && instance.DBInstanceStatus == "stopped") {
                console.log(
                  "DB instance " +
                    instance.DBInstanceIdentifier +
                    " that can be started."
                );
                await startRdsInstance(instance.DBInstanceIdentifier);
                console.log(
                  "DB instance " +
                    instance.DBInstanceIdentifier +
                    " is started."
                );
                startedInstanceCount++;
              }

              if (
                action == "stop" &&
                instance.DBInstanceStatus == "available"
              ) {
                console.log(
                  "DB instance " +
                    instance.DBInstanceIdentifier +
                    " that can be stopped."
                );
                await stopRdsInstance(instance.DBInstanceIdentifier);
                console.log(
                  "DB instance " +
                    instance.DBInstanceIdentifier +
                    " is stopped."
                );
                stoppedInstanceCount++;
              }
            }
          }
        }
      } catch (err) {
        reject(err);
      }
    }

    if (startedInstanceCount == 0 && action == "start")
      console.log("Found 0 RDS instance that can be started.");
    if (stoppedInstanceCount == 0 && action == "stop")
      console.log("Found 0 RDS instance that can be stopped.");

    try {
      console.log("=== Get RDS cluster list.");
      clusterList = await getRdsClusterList();
    } catch (err) {
      reject(err);
    }

    for (let cluster of clusterList) {
      try {
        if (cluster.Engine.includes("aurora")) {
          let tags = await getRdsResouceTags(cluster.DBClusterArn);

          for (let tag of tags) {
            if (tag.Key == tagStartStop) {
              console.log(
                "Current status of DB cluster " +
                  cluster.DBClusterIdentifier +
                  " is " +
                  cluster.Status
              );

              if (action == "start" && cluster.Status == "stopped") {
                console.log(
                  "DB cluster " +
                    cluster.DBClusterIdentifier +
                    " that can be started."
                );
                await startRdsCluster(cluster.DBClusterIdentifier);
                console.log(
                  "DB cluster " + cluster.DBClusterIdentifier + " is started."
                );
                startedClusterCount++;
              }

              if (action == "stop" && cluster.Status == "available") {
                console.log(
                  "DB cluster " +
                    cluster.DBClusterIdentifier +
                    " that can be stopped."
                );
                await stopRdsCluster(cluster.DBClusterIdentifier);
                console.log(
                  "DB cluster " + cluster.DBClusterIdentifier + " is stopped."
                );
                stoppedClusterCount++;
              }
            }
          }
        }
      } catch (err) {
        reject(err);
      }
    }

    if (startedClusterCount == 0 && action == "start")
      console.log("Found 0 RDS cluster that can be started.");
    if (stoppedClusterCount == 0 && action == "stop")
      console.log("Found 0 RDS cluster that can be stopped.");

    resolve("Method startStopRdsDB() is done.");
  });
}

function getRdsInstanceList() {
  return new Promise((resolve, reject) => {
    let params = null;

    rds.describeDBInstances(params, function(err, data) {
      if (err) {
        reject(err);
      } else {
        resolve(data.DBInstances);
      }
    });
  });
}

function getRdsResouceTags(instanceArn) {
  return new Promise((resolve, reject) => {
    let params = {
      ResourceName: instanceArn
    };

    rds.listTagsForResource(params, function(err, data) {
      if (err) {
        reject(err);
      } else {
        resolve(data.TagList);
      }
    });
  });
}

function startRdsInstance(dbInstanceIdentifier) {
  return new Promise((resolve, reject) => {
    let params = {
      DBInstanceIdentifier: dbInstanceIdentifier
    };

    rds.startDBInstance(params, function(err, data) {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

function stopRdsInstance(dbInstanceIdentifier) {
  return new Promise((resolve, reject) => {
    let params = {
      DBInstanceIdentifier: dbInstanceIdentifier
    };

    rds.stopDBInstance(params, function(err, data) {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

function getRdsClusterList() {
  return new Promise((resolve, reject) => {
    let params = null;

    rds.describeDBClusters(params, function(err, data) {
      if (err) {
        reject(err);
      } else {
        resolve(data.DBClusters);
      }
    });
  });
}

function startRdsCluster(dbClusterIdentifier) {
  return new Promise((resolve, reject) => {
    let params = {
      DBClusterIdentifier: dbClusterIdentifier
    };

    rds.startDBCluster(params, function(err, data) {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

function stopRdsCluster(dbClusterIdentifier) {
  return new Promise((resolve, reject) => {
    let params = {
      DBClusterIdentifier: dbClusterIdentifier
    };

    rds.stopDBCluster(params, function(err, data) {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}
